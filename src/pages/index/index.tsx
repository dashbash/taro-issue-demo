import { View, Text } from "@tarojs/components";
import Taro, { useLoad } from "@tarojs/taro";
import "./index.less";
import { foo } from "../../foo";

export default function Index() {
  useLoad(() => {
    console.log("Page loaded.");
  });

  return (
    <View className="index">
      <View
        onClick={() => {
          /**
           * 同时打开下面这行代码的注释与pages/test1/index.tsx下的这行注释，可以在微信小程序开发工具中看到页面白屏，检查元素发现只渲染了一个空的<page>
           */
          // foo();
          Taro.navigateTo({
            url: "/pages/test1/index",
          });
        }}
      >
        click to test page
      </View>
    </View>
  );
}
