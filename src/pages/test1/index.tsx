import { View, Text } from "@tarojs/components";
import { useLoad } from "@tarojs/taro";
import "./index.less";
import { foo } from "../../foo";

export default function Test1() {
  useLoad(() => {
    console.log("Page loaded.");
  });

  return (
    <View className="test1">
      <View
        onClick={() => {
          /**
           * 同时打开下面这行代码的注释与pages/index/index.tsx下的这行注释，可以在微信小程序开发工具中看到页面白屏，检查元素发现只渲染了一个空的<page>
           */
          foo();
        }}
      >
        this is test page
      </View>
    </View>
  );
}
